package org.mik.zoo.livingbeing;

public abstract class AbstractLivingBeing implements LivingBeing {

	private Integer id;
	private String scientificName;
	private String instanceName;
	private String imageURL;

	public AbstractLivingBeing(String scientificName) {
		this(scientificName, null, null);
	}

	public AbstractLivingBeing(String scientificName, String instanceName, String imageURL) {
		this.scientificName = scientificName;
		this.instanceName = instanceName;
		this.imageURL = imageURL;
	}

	@Override
	public Integer getId() {
		return id;
	}
	@Override
	public void setId(Integer id) {
		this.id = id;
	}

	@Override
	public String getScientificName() {
		return this.scientificName;
	}

	public void setScientificName(String scientificName) {
		this.scientificName = scientificName;
	}

	@Override
	public String getInstanceName() {
		return this.instanceName;
	}

	public void setInstanceName(String instanceName) {
		this.instanceName = instanceName;
	}

	@Override
	public String getImageURL() {
		return this.imageURL;
	}

	public void setImageURL(String imageURL) {
		this.imageURL = imageURL;
	}
}

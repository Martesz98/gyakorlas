package org.mik.zoo.livingbeing;

import org.mik.zoo.livingbeing.plant.Plant;

public interface LivingBeing {



	public String getScientificName();

	public String getInstanceName();

	public String getImageURL();

	public Integer getId();
	
	public void setId(Integer id);

}

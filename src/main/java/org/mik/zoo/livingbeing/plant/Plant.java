package org.mik.zoo.livingbeing.plant;

import org.mik.zoo.livingbeing.LivingBeing;

public interface Plant extends LivingBeing {

	public int getHeight();
}

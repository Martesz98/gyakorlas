package org.mik.zoo.livingbeing.plant;

import org.mik.zoo.livingbeing.AbstractLivingBeing;

public abstract class AbstractPlant extends AbstractLivingBeing implements Plant {

	private int height;

	public AbstractPlant(String scientificName) {
		this(scientificName, 0);

	}

	public AbstractPlant(String scientificName, int height) {
		this(scientificName, null, null, height);
	}

	public AbstractPlant(String scientificName, String instanceName, String imageURL, int height) {
		super(scientificName, instanceName, imageURL);
		this.height = height;
	}

	@Override
	public int getHeight() {
		return this.height;

	}

	public void setHeight(int height) {
		this.height = height;

	}

}

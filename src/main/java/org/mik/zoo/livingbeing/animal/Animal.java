package org.mik.zoo.livingbeing.animal;

import org.mik.zoo.livingbeing.LivingBeing;

public interface Animal extends LivingBeing {

	public AnimalType getAnimalType();

	public int getNumberOfLegs();

	public int getNumberOfTeeth();

	public int getWeight();
}
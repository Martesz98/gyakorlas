package org.mik.zoo.livingbeing.animal.mammal;

import org.mik.zoo.livingbeing.animal.AbstractAnimal;
import org.mik.zoo.livingbeing.animal.AnimalType;

public abstract class AbstractMammal extends AbstractAnimal implements Mammal {

	private int lengthOfHair;

	public AbstractMammal(String scientificName, AnimalType animalType) {
		this(scientificName, null, null, animalType);
	}

	public AbstractMammal(String scientificName, String instanceName, String imageURL, AnimalType animalType) {
		this(scientificName, instanceName, imageURL, animalType, 0, 0, 0, 0);
	}

	public AbstractMammal(String scientificName, String instanceName, String imageURL, AnimalType animalType,
			int numberOfLegs, int numberOfTeeth, int weight, int lengthOfHair) {
		super(scientificName, instanceName, imageURL, animalType, numberOfLegs, numberOfTeeth, weight);
		this.lengthOfHair = lengthOfHair;
	}

	@Override
	public int getLengthOfHair() {
		return this.lengthOfHair;
	}

	public void setLengthOfHair(int lengthOfHair) {
		this.lengthOfHair = lengthOfHair;
	}
}

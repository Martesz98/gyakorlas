package org.mik.zoo.livingbeing.animal;

import org.mik.zoo.livingbeing.AbstractLivingBeing;

public abstract class AbstractAnimal extends AbstractLivingBeing implements Animal {

	private AnimalType animalType;
	private int numberOfLegs;
	private int numberOfTeeth;
	private int weight;

	public AbstractAnimal(String scientificName, AnimalType animalType) {
		this(scientificName, null, null, animalType);
	}

	public AbstractAnimal(String scientificName, String instanceName, String imageURL, AnimalType animalType) {
		this(scientificName, instanceName, imageURL, animalType, 0, 0, 0);
	}

	public AbstractAnimal(String scientificName, String instanceName, String imageURL, AnimalType animalType,
			int numberOfLegs, int numberOfTeeth, int weight) {
		super(scientificName, instanceName, imageURL);
		this.animalType = animalType;
		this.numberOfLegs = numberOfLegs;
		this.numberOfTeeth = numberOfTeeth;
		this.weight = weight;
	}

	@Override
	public AnimalType getAnimalType() {
		return this.animalType;
	}

	@Override
	public int getNumberOfLegs() {
		return this.numberOfLegs;
	}

	public void setNumberOfLegs(int numberOfLegs) {
		this.numberOfLegs = numberOfLegs;
	}

	@Override
	public int getNumberOfTeeth() {
		return this.numberOfTeeth;
	}

	public void setNumberOfTeeth(int numberOfTeeth) {
		this.numberOfTeeth = numberOfTeeth;
	}

	@Override
	public int getWeight() {
		return this.weight;
	}

	public void setWeight(int weight) {
		this.weight = weight;
	}
}

package org.mik.zoo.livingbeing.animal.mammal;

import org.mik.zoo.livingbeing.animal.AnimalType;

public class Elephant extends AbstractMammal {

	public static final String SCIENTIFIC_NAME = "Loxodonta africana"; //$NON-NLS-1$
	public static final AnimalType TYPE = AnimalType.HERBIVOROUS;

	public Elephant() {
		super(SCIENTIFIC_NAME, TYPE);
	}

	public Elephant(String instanceName, String imageURL) {
		super(SCIENTIFIC_NAME, instanceName, imageURL, TYPE);
	}

	public Elephant(String instanceName, String imageURL, int numberOfLegs, int numberOfTeeth, int weight,
			int lengthOfHair) {
		super(SCIENTIFIC_NAME, instanceName, imageURL, TYPE, numberOfLegs, numberOfTeeth, weight, lengthOfHair);
	}
}

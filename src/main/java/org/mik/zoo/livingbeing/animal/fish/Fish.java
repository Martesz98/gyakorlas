package org.mik.zoo.livingbeing.animal.fish;

import org.mik.zoo.livingbeing.animal.Animal;

public interface Fish extends Animal {

	public int getNumberOfFins();
}
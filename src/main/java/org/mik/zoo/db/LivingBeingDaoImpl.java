package org.mik.zoo.db;

import java.net.URI;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.mik.zoo.factory.DataBaseFactory;
import org.mik.zoo.livingbeing.LivingBeing;
import org.mik.zoo.livingbeing.animal.AbstractAnimal;
import org.mik.zoo.livingbeing.animal.Animal;
import org.mik.zoo.livingbeing.animal.bird.AbstractBird;
import org.mik.zoo.livingbeing.animal.bird.Bird;
import org.mik.zoo.livingbeing.animal.fish.AbstractFish;
import org.mik.zoo.livingbeing.animal.fish.Fish;
import org.mik.zoo.livingbeing.animal.mammal.AbstractMammal;
import org.mik.zoo.livingbeing.animal.mammal.Elephant;
import org.mik.zoo.livingbeing.animal.mammal.Mammal;
import org.mik.zoo.livingbeing.plant.AbstractPlant;
import org.mik.zoo.livingbeing.plant.Plant;

public class LivingBeingDaoImpl implements LivingBeingDao {

	private static final Logger LOG = LogManager.getLogger(LivingBeingDaoImpl.class);

	private static final String TABLE_NAME = "living_being"; //$NON-NLS-1$
	private static final String COL_ID = "id"; //$NON-NLS-1$
	private static final String COL_SCIENTIFIC_NAME = "scientific_name"; //$NON-NLS-1$
	private static final String COL_INSTANCE_NAME = "instance_name"; //$NON-NLS-1$
	private static final String COL_IMAGE_URL = "image_url"; //$NON-NLS-1$
	private static final String COL_NUMBER_OF_LEGS = "number_of_legs"; //$NON-NLS-1$
	private static final String COL_NUMBER_OF_TEETH = "number_of_teeth"; //$NON-NLS-1$
	private static final String COL_WEIGHT = "weight"; //$NON-NLS-1$
	private static final String COL_WING_LENGTH_ = "wing_length"; //$NON-NLS-1$
	private static final String COL_NUMBER_OF_FINS = "number_of_fins"; //$NON-NLS-1$
	private static final String COL_LENGTH_OF_HAIR = "length_of_hair"; //$NON-NLS-1$
	private static final String COL_HEIGHT = "height"; //$NON-NLS-1$
	private static final String COL_DECIDUOUS = "deciduous"; //$NON-NLS-1$
	private static final String COL_COLOR = "color"; //$NON-NLS-1$

	private String initFileName;

	public LivingBeingDaoImpl(String initFileName) {
		super();
		this.initFileName = initFileName;
	}

	public LivingBeing findOne(Integer id) {
		LOG.debug("fondOne(); id: " + id);

		LivingBeing livingBeing = null;

		try (Connection connection = this.createConnection()) {
			String sql = "SELECT *FROM " + "WHERE " + COL_ID + "=?";
			try (PreparedStatement statement = connection.prepareStatement(sql)) {
				statement.setInt(1, id);

				try (ResultSet resultSet = statement.executeQuery()) {
					if (resultSet.next()) {
						livingBeing = this.createInstance(resultSet);

					}
				}
			}

		} catch (Exception e) {
			LOG.error("", e); //$NON-NLS-1$
		}

		return livingBeing;
	}

	@Override
	public List<LivingBeing> findAll() {
		List<LivingBeing> livingBeings = new ArrayList<>();

		try (Connection connection = this.createConnection()) {

			LOG.info("Loading data from database");

			try (Statement statement = connection.createStatement()) {
				try (ResultSet resultSet = statement.executeQuery("SELECT * FROM " + TABLE_NAME)) {
					while (resultSet.next()) {

						LivingBeing livingBeing = this.createInstance(resultSet);

						if (livingBeing != null) {

							livingBeings.add(livingBeing);
						}
					}
				}
			}
		} catch (Exception e) {
			LOG.error("", e); //$NON-NLS-1$
		}

		return livingBeings;
	}

	@Override
	public List<LivingBeing> findByScientificName(String name) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public List<LivingBeing> findByInstanceName(String name) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public LivingBeing persist(LivingBeing livingBeing) {
		if (livingBeing.getId() == null) {
			return this.insert(livingBeing);

		} else {
			return this.update(livingBeing);
		}
	}

	@Override
	public boolean delete(LivingBeing livingBeing) {
		try (Connection connection = this.createConnection()) {
			String sql = "DELETE FROM" + TABLE_NAME + " WHERE " + COL_ID + "=?";

			try (PreparedStatement statement = connection.prepareStatement(sql)) {
				statement.setInt(1, livingBeing.getId());

				if (statement.executeUpdate() == 1) {
					return true;
				}
			}
		} catch (Exception e) {
			LOG.error("", e); //$NON-NLS-1$
		}
		
		return false;
	}

	private LivingBeing createInstance(ResultSet resultSet) {
		LivingBeing livingBeing = null;

		try {
			// extract data from result set
			int id = resultSet.getInt(COL_ID);
			String scientificName = resultSet.getString(COL_SCIENTIFIC_NAME);
			String instanceName = resultSet.getString(COL_INSTANCE_NAME);
			String imageUrl = resultSet.getString(COL_IMAGE_URL);
			int numberOfLegs = resultSet.getInt(COL_NUMBER_OF_LEGS);
			int numberOfTeeth = resultSet.getInt(COL_NUMBER_OF_TEETH);
			int weight = resultSet.getInt(COL_WEIGHT);
			int wingLength = resultSet.getInt(COL_WING_LENGTH_);
			int numberOfFins = resultSet.getInt(COL_NUMBER_OF_FINS);
			int lengthOfHair = resultSet.getInt(COL_LENGTH_OF_HAIR);
			int height = resultSet.getInt(COL_HEIGHT);
			boolean deciduous = resultSet.getBoolean(COL_DECIDUOUS);
			String color = resultSet.getString(COL_COLOR);

			// create living being instance
			switch (scientificName) {
			case Elephant.SCIENTIFIC_NAME:
				livingBeing = new Elephant(instanceName, imageUrl);
				break;

			default:
				LOG.warn("Unknown living being: " + scientificName);

				break;
			}
			if (livingBeing != null) {
				livingBeing.setId(id);

				if (livingBeing instanceof AbstractAnimal) {
					((AbstractAnimal) livingBeing).setNumberOfLegs(numberOfLegs);
					((AbstractAnimal) livingBeing).setNumberOfTeeth(numberOfTeeth);
					((AbstractAnimal) livingBeing).setWeight(weight);

					if (livingBeing instanceof AbstractMammal) {

						((AbstractMammal) livingBeing).setLengthOfHair(lengthOfHair);
					} else if (livingBeing instanceof AbstractBird) {

						((AbstractBird) livingBeing).setWingLenght(wingLength);

					} else if (livingBeing instanceof AbstractFish) {

						((AbstractFish) livingBeing).setNumberOfFins(numberOfFins);
					}
				} else if (livingBeing instanceof AbstractPlant) {
					((AbstractPlant) livingBeing).setHeight(height);
				}

				// TODO tree
				// TODO flower
			}

			// TODO other columns

		} catch (

		SQLException e) {
			LOG.error("", e); //$NON-NLS-1$
		}

		return livingBeing;
	}

	@Override
	public void initDb() {
		LOG.debug("checkTable()");

		try (Connection connection = this.createConnection()) {

			try (Statement statement = connection.createStatement()) {
				try (ResultSet resultSet = statement.executeQuery("SELECT * FROM information_schema.tables")) { //$NON-NLS-1$
					while (resultSet.next()) {
						String tableName = resultSet.getString("TABLE_NAME"); //$NON-NLS-1$

						if (tableName.equalsIgnoreCase(TABLE_NAME)) {
							return;
						}
					}
				}
			}

			LOG.info("Database is empy");

			URI uri = this.getClass().getResource(this.initFileName).toURI();
			Path path = Paths.get(uri);
			for (String sql : Files.readAllLines(path)) {
				try (Statement statement = connection.createStatement()) {
					statement.executeUpdate(sql);
				}
			}
		} catch (Exception e) {
			LOG.error("", e);
		}
	}

	@SuppressWarnings("static-method")
	private Connection createConnection() throws Exception {
		Class.forName("org.hsqldb.jdbc.JDBCDriver"); //$NON-NLS-1$
		String url = "jdbc:hsqldb:zoodb"; //$NON-NLS-1$

		return DriverManager.getConnection(url, "sa", ""); //$NON-NLS-1$ //$NON-NLS-2$
	}

	private LivingBeing insert(LivingBeing livingBeing) {
		StringBuilder stringBuilder = new StringBuilder();
		stringBuilder.append("INSERT INTO ");
		stringBuilder.append(TABLE_NAME).append('(');
		stringBuilder.append(COL_SCIENTIFIC_NAME).append(',');
		stringBuilder.append(COL_INSTANCE_NAME).append(',');
		stringBuilder.append(COL_IMAGE_URL).append(',');
		stringBuilder.append(COL_NUMBER_OF_LEGS).append(',');
		stringBuilder.append(COL_NUMBER_OF_TEETH).append(',');
		stringBuilder.append(COL_WEIGHT).append(',');
		stringBuilder.append(COL_WING_LENGTH_).append(',');
		stringBuilder.append(COL_NUMBER_OF_FINS).append(',');
		stringBuilder.append(COL_LENGTH_OF_HAIR).append(',');
		stringBuilder.append(COL_HEIGHT).append(',');
		stringBuilder.append(COL_DECIDUOUS).append(',');
		stringBuilder.append(COL_COLOR).append(')');
		stringBuilder.append(" VALUES(?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)");

		LOG.debug(stringBuilder.toString());

		try (Connection connection = this.createConnection()) {
			try (PreparedStatement statement = connection.prepareStatement(stringBuilder.toString(),
					Statement.RETURN_GENERATED_KEYS)) {

				this.setParameters(statement, livingBeing);

				statement.executeUpdate();

				try (ResultSet resultset = statement.getGeneratedKeys()) {
					if (resultset.next()) {
						int id = resultset.getInt(1);

						livingBeing.setId(id);

						return livingBeing;
					}
				}
			}
		} catch (Exception e) {
			LOG.error("", e);

		}
		return null;

	}

	private LivingBeing update(LivingBeing livingBeing) {
		// TODO Auto-generated method stub
		return null;
	}

	private void setParameters(PreparedStatement statement, LivingBeing livingBeing) throws SQLException {
		statement.setString(1, livingBeing.getScientificName());
		statement.setString(2, livingBeing.getInstanceName());
		statement.setString(3, livingBeing.getImageURL());

		if (livingBeing instanceof Animal) {
			Animal animal = (Animal) livingBeing;

			statement.setInt(4, animal.getNumberOfLegs());
			statement.setInt(5, animal.getNumberOfTeeth());
			statement.setInt(6, animal.getWeight());

			// Bird
			if (livingBeing instanceof Bird) {
				statement.setInt(7, ((Bird) livingBeing).getWingLength());
			} else {
				statement.setObject(7, null);
			}

			// Fish
			if (livingBeing instanceof Fish) {
				statement.setInt(8, ((Fish) livingBeing).getNumberOfFins());
			} else {
				statement.setObject(8, null);
			}

			// Mammal
			if (livingBeing instanceof Mammal) {
				statement.setInt(9, ((Mammal) livingBeing).getLengthOfHair());
			} else {
				statement.setObject(9, null);
			}
		} else {
			statement.setObject(4, null);
			statement.setObject(5, null);
			statement.setObject(6, null);
			statement.setObject(7, null);
			statement.setObject(8, null);
			statement.setObject(9, null);
		}

		if (livingBeing instanceof Plant) {
			statement.setInt(10, ((Plant) livingBeing).getHeight());

			// TODO check
			statement.setObject(11, null);
			statement.setObject(12, null);
		} else {
			statement.setObject(10, null);
		}
	}

}

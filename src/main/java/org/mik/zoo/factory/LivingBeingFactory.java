package org.mik.zoo.factory;

import java.util.List;

import org.mik.zoo.livingbeing.LivingBeing;

public interface LivingBeingFactory {

	public List<LivingBeing> loadData() throws Exception;
}

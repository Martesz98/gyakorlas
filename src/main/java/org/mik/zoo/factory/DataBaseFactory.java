package org.mik.zoo.factory;

import java.net.URI;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.mik.zoo.livingbeing.LivingBeing;
import org.mik.zoo.livingbeing.animal.mammal.Elephant;

public class DataBaseFactory implements LivingBeingFactory {

	private static final Logger LOG = LogManager.getLogger(DataBaseFactory.class);

	private static final String TABLE_NAME = "living_being"; //$NON-NLS-1$
	private static final String COL_ID = "id"; //$NON-NLS-1$
	private static final String COL_SCIENTIFIC_NAME = "scientific_name"; //$NON-NLS-1$
	private static final String COL_INSTANCE_NAME = "instance_name"; //$NON-NLS-1$
	private static final String COL_IMAGE_URL = "image_url"; //$NON-NLS-1$

	private String initFileName;

	public DataBaseFactory(String initFileName) {
		super();
		this.initFileName = initFileName;
	}

	@Override
	public List<LivingBeing> loadData() throws Exception {
		List<LivingBeing> livingBeings = new ArrayList<>();

		try (Connection connection = this.createConnection()) {
			this.checkTable(connection);

			LOG.info("Loading data from database");

			try (Statement statement = connection.createStatement()) {
				try (ResultSet resultSet = statement.executeQuery("SELECT * FROM " + TABLE_NAME)) {
					while (resultSet.next()) {

						this.createInstance(resultSet);
					}
				}
			}
		}

		return livingBeings;
	}

	private LivingBeing createInstance(ResultSet resultSet) {
		LivingBeing livingBeing = null;

		try {
			int id = resultSet.getInt(COL_ID);
			String scientificName = resultSet.getString(COL_SCIENTIFIC_NAME);
			String instanceName = resultSet.getString(COL_INSTANCE_NAME);
			String imageUrl = resultSet.getString(COL_IMAGE_URL);

			switch (scientificName) {
			case Elephant.SCIENTIFIC_NAME:
				livingBeing = new Elephant(instanceName, imageUrl);
				break;

			default:
				LOG.warn("Unknown living being: " + scientificName);

				break;
			}
			if (livingBeing != null) {
				livingBeing.setId(id);
			}

		} catch (SQLException e) {
			LOG.error("", e); //$NON-NLS-1$
		}

		return livingBeing;
	}

	private void checkTable(Connection connection) throws Exception {
		LOG.debug("checkTable()");

		try (Statement statement = connection.createStatement()) {
			try (ResultSet resultSet = statement.executeQuery("SELECT * FROM information_schema.tables")) { //$NON-NLS-1$
				while (resultSet.next()) {
					String tableName = resultSet.getString("TABLE_NAME"); //$NON-NLS-1$

					if (tableName.equalsIgnoreCase(TABLE_NAME)) {
						return;
					}
				}
			}
		}

		LOG.info("Database is empy");

		URI uri = this.getClass().getResource(this.initFileName).toURI();
		Path path = Paths.get(uri);
		for (String sql : Files.readAllLines(path)) {
			try (Statement statement = connection.createStatement()) {
				statement.executeUpdate(sql);
			}
		}
	}

	@SuppressWarnings("static-method")
	private Connection createConnection() throws Exception {
		Class.forName("org.hsqldb.jdbc.JDBCDriver"); //$NON-NLS-1$
		String url = "jdbc:hsqldb:zoodb"; //$NON-NLS-1$

		return DriverManager.getConnection(url, "sa", ""); //$NON-NLS-1$ //$NON-NLS-2$
	}

}

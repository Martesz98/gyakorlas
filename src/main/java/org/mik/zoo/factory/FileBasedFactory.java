package org.mik.zoo.factory;

import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

import org.mik.zoo.livingbeing.LivingBeing;
import org.mik.zoo.livingbeing.animal.mammal.Elephant;

public class FileBasedFactory implements LivingBeingFactory {

	private static final String DELIMETER = ";"; //$NON-NLS-1$

	private String fileName;

	public FileBasedFactory(String fileName) {
		super();
		this.fileName = fileName;
	}

	@Override
	public List<LivingBeing> loadData() throws Exception {
		List<LivingBeing> livingBeings = new ArrayList<>();

		try (Scanner scanner = new Scanner(Paths.get(this.fileName))) {
			// first line
			if (!scanner.hasNextLine()) {
				return livingBeings;
			}

			scanner.nextLine();

			// iteration
			while (scanner.hasNextLine()) {
				String line = scanner.nextLine();

				if (line != null && !line.isEmpty()) {
					LivingBeing livingBeing = this.processLine(line);
					
					if(livingBeing != null) {
						livingBeings.add(livingBeing);
					}
				}
			}
		}

		return livingBeings;
	}

	@SuppressWarnings("static-method")
	private LivingBeing processLine(String line) {
		String[] data = line.split(DELIMETER);

		switch (data[0]) {
		case Elephant.SCIENTIFIC_NAME:
			return new Elephant(data[1], data[2]);

		default:
			System.out.println("Unknown living being: " + data[0]); //$NON-NLS-1$
			break;
		}
		
		return null;
	}

}








package org.mik.zoo;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.mik.zoo.db.LivingBeingDao;
import org.mik.zoo.db.LivingBeingDaoImpl;
import org.mik.zoo.livingbeing.LivingBeing;
import org.mik.zoo.livingbeing.animal.Animal;
import org.mik.zoo.livingbeing.animal.AnimalType;

/**
 * Main class
 *
 * @author
 *
 */
public class App {

	private static final Logger LOG = LogManager.getLogger(App.class);
	/**
	 * Name of the data file
	 */
	private static final String FILE_NAME = "data.txt"; //$NON-NLS-1$

	private static final String DB_INIT_FILE_NAME = "/init.sql"; //$NON-NLS-1$

	/**
	 * All living being
	 */
	private List<LivingBeing> livingBeings;
	/**
	 * 
	 */
	private Map<AnimalType, List<Animal>> animalsByType;

	/**
	 * Constructor for the application
	 */
	public App() {
		this.livingBeings = new ArrayList<>();
		this.animalsByType = new HashMap<>();
	}

	/**
	 * Entry point of the application
	 *
	 * @param args
	 *            arguments
	 */
	public static void main(String[] args) {
		new App().start();
	}

	/**
	 * Invoke methods
	 */
	private void start() {
		this.loadData();
		this.organizeAnimals();
		this.printAnswers();
		this.printAnimalsByType();
	}

	/**
	 * Load data
	 */
	private void loadData() {
		LivingBeingDao dao = new LivingBeingDaoImpl(DB_INIT_FILE_NAME);
		dao.initDb();

		this.livingBeings.addAll(dao.findAll());
	}

	/**
	 * Answer the questions of the kids visiting the zoo
	 */
	@SuppressWarnings("boxing")
	private void printAnswers() {
		System.out.println("\nQuestions - answers\n"); //$NON-NLS-1$
		System.out.println("How many animals are there here?"); //$NON-NLS-1$
		System.out.println(String.format("animal count: %d", this.countAnimals())); //$NON-NLS-1$
		System.out.println("How many elephants are there here?"); //$NON-NLS-1$

		System.out.println("Are there predators here?"); //$NON-NLS-1$

		System.out.println("Is Tux here?"); //$NON-NLS-1$
	}

	/**
	 * Count animals
	 *
	 * @return number of animals
	 */
	private int countAnimals() {
		int animalCount = 0;

		for (LivingBeing livingBeing : this.livingBeings) {
			if (livingBeing instanceof Animal) {
				animalCount++;
			}
		}

		return animalCount;
	}

	/**
	 * Count elephants
	 *
	 * @return number of elephants
	 */
	// TODO homework
	private int countElephants() {
		return 0;
	}

	/**
	 * Find a living being by the given name
	 *
	 * @param name
	 *            the name
	 * @return LivingBeing instance if found or null, if the name was null or no
	 *         LivingBeing for this name
	 */
	private LivingBeing findByName(String name) {
		return null;
	}

	/**
	 * Collect animals by their types
	 */
	private void organizeAnimals() {
		for (LivingBeing livingBeing : this.livingBeings) {
			if (livingBeing instanceof Animal) {
				Animal animal = (Animal) livingBeing;

				AnimalType animalType = animal.getAnimalType();
				List<Animal> list = this.animalsByType.get(animalType);

				if (list == null) {
					list = new ArrayList<>();
				}
				list.add(animal);

				this.animalsByType.put(animalType, list);
				// TODO fix
			}
		}
	}

	/**
	 * Printing animals by their types
	 */
	private void printAnimalsByType() {
		System.out.println("\nAnimals by type\n"); //$NON-NLS-1$
		System.out.println(this.animalsByType);
	}
}
